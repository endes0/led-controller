#include <Arduino.h>
#include <ArduinoJson.h>
#include <FastLED.h>
#include <LEDEffect.h>

#include <EEPROM.h>
#ifdef i2c
#include <Wire.h>
#endif

#include <commander.led>

void setup()
{
  #ifdef i2c
  Wire.begin();
  #endif
  Serial.begin(9600);
  delay(3000);
  #ifdef i2c
  write_start();
  #endif

  strip.begin(&FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS));

  //current_scene = get_start_scene();
  change_scene();
}

void loop()
{
  commander_loop();
  leds_loop();
}